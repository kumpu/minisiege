#!/bin/python2

from module_strings import strings
from module_scenes import scenes

res = ""
for scene in scenes:
	if scene[0].startswith("mp_"):
		res += str(scenes.index(scene)) + ' => ["' + scene[0] + '", "'
		
		for string in strings:
			if scene[0] == "mp_countryside_fog":
				res += "Countryside (Fog)" + '"],\n'
				break;
			elif string[0] == scene[0]:
				if string[1].startswith("custom_map_"):
					res += "Custom Map " + string[1][len("custom_map_"):]
				else:
					res += string[1]
				res += '"],\n'
				break;
				
print res
