#!/bin/bash
source config_shell.sh

echo Uploading to control panel...

sftp -P "$live_port" "$live_address" <<END

cd "$live_path"

put header_*.py
put ID_*.py
put module_*.py
put process_*.py
put variables.txt
put .build_no
put -r lua
put -r brf
END
