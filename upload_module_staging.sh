#!/bin/bash
source config_shell.sh

echo Uploading to staging control panel...

sftp -P "$stage_port" "$stage_address" <<END

cd "$stage_path"

put header_*.py
put ID_*.py
put module_*.py
put process_*.py
put variables.txt
put .build_no
put -r lua
put -r brf
END
